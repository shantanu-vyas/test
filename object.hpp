/** 
 * @file object.hpp
 * @author shantanu vyas
 * @version 1.0
 * @section LICENSE
 * tbd
 * @section description
 * testing the wiki system
 **/

#ifndef __object__
#define __object__


class object
{
  /** @brief this is our int **/
  int a;
  /** @brief this is our double **/
  int b;

  
  object();
  ~object();

  void doStuff();
  void stopDoingStuff();
}

#endif
