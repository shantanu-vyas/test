/** 
 * object class
 * @file object.cpp
 * @brief object class definition
 * @author shantanu vyas
 * @version 1.0
 * @section LICENSE
 * tbd
 * @section description
 * testing the wiki system
 **/

#include "object.hpp"
class object
{

  /**
   *   @brief initialize object
   *
   *   @note this does nothing
   *
   *   @todo make it do something
   *   
   */

  object::object()
  {
    
  }

  //--------------------------------------------------------------------------
  //
  //  ~object (destructor)
  /**
   *   @brief deinitialize object
   *
   *   @note this does nothing
   *
   *   @todo make it do something
   *   
   *///-----------------------------------------------------------------------


  object::~object()
  {
    
  }

  //--------------------------------------------------------------------------
  //
  //  doStuff
  /**
   *   @brief does stuff
   *
   *   @note does stuff
   *
   *   @todo make it do more
   *   
   *///-----------------------------------------------------------------------


  void object::doStuff()
  {
    
  }

  //--------------------------------------------------------------------------
  //
  //  stopDoingStuff
  /**
   *   @brief stops does stuff
   *
   *   @note stops doing stuff you were previously doing
   *
   *   @todo make it stop doing stuff  more
   *   
   *///-----------------------------------------------------------------------

  void object::stopDoingStuff()
  {
    
  }
  
}
